package events

import (
	"gitlab.com/ignitionrobotics/cloudsim/api/metadata"
	"google.golang.org/protobuf/types/known/timestamppb"
	"time"
)

const (
	actionExpired   = "expired"
	actionFailed    = "failed"
	actionRequested = "requested"
	actionRunning   = "running"
	actionScheduled = "scheduled"
	actionSucceeded = "succeeded"
	actionFinished  = "finished"
	actionRestarted = "restarted"
)

// newMetadata initializes a new event Metadata for the given action.
func newMetadata(action string) *metadata.Event {
	return &metadata.Event{
		Action:    action,
		CreatedAt: timestamppb.New(time.Now()),
	}
}

// NewSimulationExpired initializes a new SimulationExpired event.
func NewSimulationExpired(gid string) *SimulationExpired {
	return &SimulationExpired{
		Metadata: newMetadata(actionExpired),
		GroupId:  gid,
	}
}

// NewSimulationFailed initializes a new SimulationFailed event.
func NewSimulationFailed(gid, reason string) *SimulationFailed {
	return &SimulationFailed{
		Metadata: newMetadata(actionFailed),
		GroupId:  gid,
		Reason:   reason,
	}
}

// NewSimulationRequested initializes a new SimulationRequested event.
func NewSimulationRequested(name, image string, scenario []byte) *SimulationRequested {
	return &SimulationRequested{
		Metadata: newMetadata(actionRequested),
		Name:     name,
		Image:    image,
		Scenario: scenario,
	}
}

// NewSimulationRunning initializes a new SimulationRunning event.
func NewSimulationRunning(gid string) *SimulationRunning {
	return &SimulationRunning{
		Metadata: newMetadata(actionRunning),
		GroupId:  gid,
	}
}

// NewSimulationScheduled initializes a new SimulationScheduled event.
func NewSimulationScheduled(gid string) *SimulationScheduled {
	return &SimulationScheduled{
		Metadata: newMetadata(actionScheduled),
		GroupId:  gid,
	}
}

// NewSimulationSucceeded initializes a new SimulationSucceeded event.
func NewSimulationSucceeded(gid string) *SimulationSucceeded {
	return &SimulationSucceeded{
		Metadata: newMetadata(actionSucceeded),
		GroupId:  gid,
	}
}

// NewSimulationFinished initializes a new SimulationFinished event.
func NewSimulationFinished(gid string, results []byte, completedAt time.Time, err *string) *SimulationFinished {
	var rs string
	if err != nil {
		rs = *err
	}
	return &SimulationFinished{
		Metadata:    newMetadata(actionFinished),
		GroupId:     gid,
		Results:     results,
		Error:       rs,
		CompletedAt: timestamppb.New(completedAt),
	}
}

// NewSimulationRestarted initializes a new SimulationRestarted event.
func NewSimulationRestarted(gid string) *SimulationRestarted {
	return &SimulationRestarted{
		Metadata: newMetadata(actionRestarted),
		GroupId:  gid,
	}
}
