build:
	@echo "Building gRPC and Protobuf in Go, TypeScript and C++"
	buf generate --template buf.gen.go.yaml
	buf generate --template buf.gen.ts.yaml
	buf generate --template buf.gen.cpp.yaml

clean:
	@echo "Removing compiled protobuf messages"
	rm -R */*.pb.go
	rm -Rf cpp/
	rm -Rf ts/
